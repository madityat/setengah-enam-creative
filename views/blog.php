<?php
    require_once('./models/portfolio.php');
    require_once('./models/blog.php');

    $connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
	$portfolio = new Portfolio($connection);
	$blog = new Blog($connection);
?>
<div class="banner">
    <div class="wrap">
        <h2>Blog</h2>
        <div class="clear"></div>
    </div>
</div>
<div class="main">
<?php
    if ($_GET['page'] == 'blog-detail') {
        echo "<div class='single-wrapper'>";
    } else {
        echo "
        <div class='project-wrapper'>
            <div class='project'>";
    }
?>
        
        <?php
            if ($_GET['page'] == 'blog') {
                include_once('./views/blog_list.php');
                echo "</div>";
            } else if ($_GET['page'] == 'blog-detail') {
                include_once('./views/blog_detail.php');
            } else {
                include_once('./views/404.php');
                echo "</div>";
            }
        ?>
        <div class="project-sidebar">
            <div class="project-list">
                <div class="search_box">
                    <form action="./blog" method="post">
                        <input type="hidden" name="page" value="blog"/>
                        <input type="text" value="Search...." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" name="search">
                        <input type="submit" name="" value="">
                    </form>
                </div>
            </div>
            <div class="project-list2" style="margin-bottom: 30px;">
                <h4>Categories</h4>
                    <ul>
                    <?php
                        $getBlogCategory = $blog->getBlogCategory();
                        while($data = $getBlogCategory->fetch_object()){
                            echo "<li><a href='./blog/category/$data->category'> ".strtoupper($data->category)." </a></li>";
                        }
                    ?>
					<div class="clear"></div>
				</ul>
            </div>
            <div class="project-list2" style="margin-bottom: 30px;">
                <h4>Archive <?php echo date("Y"); ?></h4>
                <ul>
                    <?php
                        for ($i=1 ; $i<=12 ; $i++) {
                            $getArchive = $blog->getArchive(null, $i);
                            $dateObj   = DateTime::createFromFormat('!m', $i);
                            $monthName = $dateObj->format('F'); // March
                            echo "<li><a href='./blog/archive/$i'> $monthName (".$getArchive->num_rows.") </a></li>";
                        }
                    ?>
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="project-list1">
				<h4>Popular Posts</h4>
				<ul>
				<?php
					$getBlog = $blog->getBlogPopular(4);
					while($data = $getBlog->fetch_object()){
						$image = explode(',', $data->image);
						$category = str_replace(' ','',$data->category);
						?>
						<li>
							<a href="./blog-detail/blog/<?php echo $data->id;?>"><img src="<?php echo $image[0];?>" alt=""></a>
							<p><a href="./blog-detail/blog/<?php echo $data->id;?>"><?php echo $data->title;?></a></p>
							<span class="likes"><span class="link"><a href="./blog-detail/blog/<?php echo $data->id;?>"><?php echo date('d F Y', strtotime($data->updatedAt));?></a></span>
							<a href="./blog-detail/blog/<?php echo $data->id;?>">
								<img src="images/heart.png" title="likes" alt="" />
							</a><?php echo $data->like;?></span>
							<div class="clear"><hr></div>
						</li>
						<?php
					}
				?>
				</ul>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>