<?php
    require_once('./models/portfolio.php');

    $connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
	$portfolio = new Portfolio($connection);
?>
<div class="banner">
	<div class="wrap">
		<h2>Portfolio</h2>
		<div class="clear"></div>
	</div>
</div>
<div class="main">
	<div class="portfolio-top">
		<div class="wrap">
			<h3>OUR MASTERPIECES</h3>
		</div>
	</div>
	<div class="container">
		<ul id="filters" class="clearfix">
			<?php
				$allTag = "";
				$getPortfolioTag = $portfolio->getPortfolioTag();
				while($data = $getPortfolioTag->fetch_object()) {
					$allTag .= str_replace(' ','',$data->tag)." ";
				}
				$status = (!isset($_GET['tag'])) ? 'active' : '';
				echo "<li style='border: 1px dotted #828282;'><span class='filter $status' data-filter='$allTag'>All</span></li>";

				$getPortfolioTag = $portfolio->getPortfolioTag();
				while($data = $getPortfolioTag->fetch_object()){
					$tag = str_replace(' ','',$data->tag);
					$status = (isset($_GET['tag']) && @$_GET['tag'] === $tag) ? 'active' : '';
					echo "<li style='border: 1px dotted #828282;'><span class='filter $status' data-filter='".$tag."' id='".$tag."'>".ucwords($data->tag,' ')."</span></li>";
				}
			?>
		</ul>
		<div class="clear"></div>
		<div id="portfoliolist" style="     " class="">
			<div class="wrapper">
				<?php
					$getPortfolio = $portfolio->getPortfolio();
					while($data = $getPortfolio->fetch_object()){
						$image = explode(',', $data->image);
						$tag = str_replace(' ','',$data->tag);
						?>
						<div class="portfolio <?php echo $tag;?>" data-cat="<?php echo $tag;?>" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">
								<a href="./portfolio-detail/portfolio/<?php echo $data->id;?>">
									<img src="<?php echo $image[0];?>" alt="Image 2">
								</a>
								<div class="links">
									<h4><a href="./portfolio-detail/portfolio/<?php echo $data->id;?>"><?php echo $data->title;?></a></h4>
									<p class="img" onclick="portfolioLike(<?php echo $data->id;?>)"><img src="images/likes.png" title="likes" alt="" /><small id="like<?php echo $data->id;?>"><?php echo $data->like;?></small></p>
									<div class="clear"></div>
									<ul>
										<li><a href="./portfolio-detail/portfolio/<?php echo $data->id;?>"><span><?php echo date('d F Y', strtotime($data->updatedAt));?></span></a></li>
									</ul>
								</div>
							</div>
						</div>
						<?php
					}
				?>
				<div class="clear"> </div>
			</div>
		</div>
	</div>
</div>
<script>
	window.addEventListener('load', function () {
		<?php
			if(isset($_GET['tag'])) {
			?>
				setTimeout(function() {
					var tag = document.getElementById('<?php echo $_GET['tag'];?>');
					tag.click();
				}, 615);
			<?php
			}
		?>
	});
</script>