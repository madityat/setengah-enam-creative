<div class="banner">
	<div class="wrap">
		<h2>404 Error</h2><div class="clear"></div>
	</div>
</div>
<div class="main">	
	<div class="page-not-found">
		<h3>404</h3>
		<h4><span><img src="images/404.png" alt=""/></span></h4>
		<a href="./home" class="home">Back to homepage</a> <div class="or">or</div>
		<a href="./blog" class="blogpage">Our Blog</a>
		<div class="clear"></div>
	</div>
</div>