<?php
	if (!isset($_GET['id'])){
		echo "<meta http-equiv = 'refresh' content = '0; url = ./portfolio' />"; 
		die;
	}
    require_once('./models/portfolio.php');

    $connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
	$portfolio = new Portfolio($connection);

	$currentPortfolio = $portfolio->getPortfolio($_GET['id']);
	if ($currentPortfolio->num_rows === 0) {
		echo "<meta http-equiv = 'refresh' content = '0; url = ./portfolio' />"; 
		die;
	}
	$currentPortfolio = $currentPortfolio->fetch_object();
?>
<link href="css/elastislide.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="js/jquery.elastislide.js"></script>
<script type="text/javascript" src="js/gallery.js"></script>
<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
		<div class="rg-image"></div>
		<div class="rg-loading"></div>
		<div class="rg-caption-wrapper">
			<div class="rg-caption" style="display:none;">
				<p></p>
			</div>
		</div>
	</div>
</script>
<div class="banner">
	<div class="wrap">
		<h2>Detail Portfolio</h2>
		<div class="clear"></div>
	</div>
</div>
<div class="main">
	<div class="project-wrapper">
		<div class="project">
			<div class="project-slider">
				<div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">	<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel">
								<ul>
								<?php
									$image = explode(',', $currentPortfolio->image);
									for ($i=0 ; $i<count($image) ; $i++) {
										echo "
											<li>
												<a class='no_link'>
													<img src='$image[$i]' data-large='$image[$i]' alt='image$i' />
												</a>
											</li>
										";
									}
								?>
								</ul>
							</div>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div>
					<!-- rg-thumbs -->
				</div>
				<!-- rg-gallery -->
			</div>
			<div class="project-bottom">
				<div class="proj-desc">
					<h4>Project Description</h4> 
					<div class="clear"></div>
					<p>
						<?php
							echo $currentPortfolio->description;
						?>
					</p>
				</div>
				<div class="related-desc">
					<h4>Recent Projects</h4>
					<div class="clear"></div>
				</div>
				<div class="gallery">
					<ul>
					<?php
						$no=0;
						$getPortfolio = $portfolio->getPortfolioLimit(3);
						while($data = $getPortfolio->fetch_object()){
							$image = explode(',', $data->image);
							?>
							<li>
								<a class="" href="./portfolio-detail/portfolio/<?php echo $data->id;?>">
									<img src="<?php echo $image[0];?>" alt="" />
									<p><?php echo $data->title;?></p>
								</a>
							</li>
							<?php
						}
					?>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="project-sidebar">
			<div class="project-list1">
				<h4>Popular Posts</h4>
				<ul>
				<?php
					$getPortfolio = $portfolio->getPortfolioPopular(4);
					while($data = $getPortfolio->fetch_object()){
						$image = explode(',', $data->image);
						$tag = str_replace(' ','',$data->tag);
						?>
						<li>
							<a href="./portfolio-detail/portfolio/<?php echo $data->id;?>"><img src="<?php echo $image[0];?>" alt=""></a>
							<p><a href="./portfolio-detail/portfolio/<?php echo $data->id;?>"><?php echo $data->title;?></a></p>
							<span class="likes"><span class="link"><a href="./portfolio-detail/portfolio/<?php echo $data->id;?>"><?php echo date('d F Y', strtotime($data->updatedAt));?></a></span>
							<a href="./portfolio-detail/portfolio/<?php echo $data->id;?>">
								<img src="images/heart.png" title="likes" alt="" />
							</a><?php echo $data->like;?></span>
							<div class="clear"><hr></div>
						</li>
						<?php
					}
				?>
				</ul>
			</div>
			<div class="project-list2">
				<h4>Tags</h4>
				<ul>
				<?php
					$getPortfolioTag = $portfolio->getPortfolioTag();
					while($data = $getPortfolioTag->fetch_object()){
						$tag = str_replace(' ','',$data->tag);
						echo "<li><a href='./portfolio/tag/$tag'> ".strtoupper($data->tag)." </a></li>";
					}
				?>
					<div class="clear"></div>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script>
	window.addEventListener('load', function () {
		// menghapus nav button diatas gambar portfolio
		$('.rg-view').remove();
	})
</script>
