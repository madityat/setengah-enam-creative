<?php
    require_once('./models/home.php');

    $connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
	$home = new Home($connection);
?>
<div class="index-banner">
	<div class="wmuSlider example1">
		<div class="wmuSliderWrapper">
			<!-- start magnific-->
			<div id="small-dialog" class="mfp-hide">
				<div class="plans_table">
					<table width="100%" cellspacing="0" class="compare_plan">
						<thead>
							<tr>
								<th class="plans-list">
									<h3>Plan Features</h3></th>
								<th class="plans-list">
									<h3>Basic</h3>
									<h4>$5<small>/month</small></h4></th>
								<th class="plans-list">
									<h3>Economy</h3>
									<h4>$9<small>/month</small></h4></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td> </td>
								<td class="order_now">
									<a href="./contact" class="scroll">
										<button>Order Now</button>
									</a>
								</td>
								<td class="order_now">
									<a href="./contact" class="scroll">
										<button>Order Now</button>
									</a>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<tr>
								<td class="plan_list_title">Web Space</td>
								<td class="price_body">Unlimited</td>
								<td class="price_body">Unlimited</td>
							</tr>
							<tr>
								<td class="plan_list_title">Bandwidth</td>
								<td class="price_body">Unlimited</td>
								<td class="price_body">Unlimited</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- end magnific-->			
			<?php
				$getSlide = $home->getSlide();
				$slideNo = 0;
				while($data = $getSlide->fetch_object()){
					?>
						<article style="position: relative; width: 100%; opacity: 1;">
							<div class="banner-wrap">
								<?php
									if ($slideNo%2 == 1) {
										echo "
											<div class='slider-right'>
												<img src='$data->image'/>
											</div>
										";
									}
								?>
								<div class="slider-left">
									<h3><?php echo $data->title_1; ?></h3>
									<h4><?php echo $data->title_2; ?></h4>
									<p class="top"><?php echo $data->sub_title_1; ?></p>
									<p class="middle"><?php echo $data->sub_title_2; ?></p>
									<p class="bottom"><?php echo $data->sub_title_3; ?></p>
									<!-- <a class="popup-with-zoom-anim" href="#small-dialog"><button class="btn btn-8 btn-8b">Order Now</button></a> -->
									<!-- <a href="./contact"><button class="btn btn-8 btn-8b">Order Now</button></a> -->
								</div>
								<?php
									if ($slideNo%2 == 0) {
										echo "
											<div class='slider-right'>
												<img src='$data->image'/>
											</div>
										";
									}
								?>
								<div class="clear"></div>
							</div>
						</article>
					<?php
					$slideNo++;
				}
			?>
		</div>
		<a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
		<ul class="wmuSliderPagination">
			<?php
				for($i=0 ; $i<$getSlide->num_rows ; $i++) {
					echo "<li><a class='no_link' >$i</a></li>";
				}
			?>
		</ul>
	</div>
	<script src="js/jquery.wmuSlider.js"></script>
	<script type="text/javascript" src="js/modernizr.custom.min.js"></script>
	<script>
		$('.example1').wmuSlider();
	</script>
</div>
<!---//End-da-slider----->
<div class="main">
	<div class="content-top">
		<div class="wrap">
			<h3>OUR SERVICES</h3>
			<h5>We Specialize in social media maintaining and this is what we offer to you.</h5>
			<div class="section group">			
			<?php
				$getService = $home->getService();
				while($data = $getService->fetch_object()){
				?>
					<div class="col_1_of_4 span_1_of_4">
						<div class="grid1">
							<img src="<?php echo $data->image; ?>" alt="" />
							<h4><?php echo $data->title; ?></h4>
						</div>
						<div class="desc">
							<p><?php echo $data->description; ?></p>
						</div>
					</div>
				<?php
				}
			?>				
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="content-middle">
		<h2><span>Our work</span></h2>
		<?php
			$getWorkTitle = $home->getWork('type', 'title');
			echo "<p>".$getWorkTitle->fetch_object()->value."</p>";
		?>
		<div id="container">
			<div id="main1">
				<ul id="tiles">
					<!-- These are our grid blocks -->
					<?php
						$getWorkLimit = $home->getWork('type', 'limit');
						$limitPhoto = $getWorkLimit->fetch_object()->value;
						$getWorkPhoto = $home->getWork('type', 'photo');
						for($i=0 ; $i<$limitPhoto ; $i++){
							$data = $getWorkPhoto->fetch_object()
							?>
								<li>
								<a href="<?php echo $data->value; ?>" rel="lightbox" class="cboxElement">
									<img src="<?php echo $data->value; ?>" width="200"/>
								</a>
							</li>
							<?php
						}
					?>					
				</ul>
			</div>
		</div>
		<link rel="stylesheet" href="css/colorbox.css">
		<!-- Include the imagesLoaded plug-in -->
		<script src="js/jquery.imagesloaded.js"></script>
		<!-- include colorbox -->
		<script src="js/jquery.colorbox-min.js"></script>
		<!-- Include the plug-in -->
		<script src="js/jquery.wookmark.js"></script>
		<!-- Once the page is loaded, initalize the plug-in. -->
		<script type="text/javascript">
			(function($) {
				$('#tiles').imagesLoaded(function() {
					// Prepare layout options.
					var options = {
						autoResize: true, // This will auto-update the layout when the browser window is resized.
						container: $('#main1'), // Optional, used for some extra CSS styling
						offset: 2, // Optional, the distance between grid items
						itemWidth: 200 // Optional, the width of a grid item
					};

					// Get a reference to your grid items.
					var handler = $('#tiles li');

					// Call the layout function.
					handler.wookmark(options);

					// Init lightbox
					$('a', handler).colorbox({
						rel: 'lightbox'
					});
				});
			})(jQuery);
		</script>
	</div>
	<div class="content-bottom">
		<h2><span>Our Clients</span></h2>
		<?php
			$getClientTitle = $home->getClient('type', 'title');
			echo "<p>".$getClientTitle->fetch_object()->value."</p>";
		?>
		<ul id="flexiselDemo3">
			<?php
				$getClient = $home->getClient('type', 'client');
				while($data = $getClient->fetch_object()){
					echo "<li><img src='$data->value' /></li>";
				}
			?>		
		</ul>
		<script type="text/javascript">
			$(window).load(function() {
				$("#flexiselDemo1").flexisel();
				$("#flexiselDemo2").flexisel({
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: {
						portrait: {
							changePoint: 480,
							visibleItems: 1
						},
						landscape: {
							changePoint: 640,
							visibleItems: 2
						},
						tablet: {
							changePoint: 768,
							visibleItems: 3
						}
					}
				});

				$("#flexiselDemo3").flexisel({
					visibleItems: 5,
					animationSpeed: 1000,
					autoPlay: true,
					autoPlaySpeed: 3000,
					pauseOnHover: true,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: {
						portrait: {
							changePoint: 480,
							visibleItems: 1
						},
						landscape: {
							changePoint: 640,
							visibleItems: 2
						},
						tablet: {
							changePoint: 768,
							visibleItems: 3
						}
					}
				});

			});
		</script>
		<script type="text/javascript" src="js/jquery.flexisel.js"></script>
	</div>
</div>