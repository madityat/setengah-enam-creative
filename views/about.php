<div class="banner">
	<div class="wrap">
		<h2>About Us</h2>
		<div class="clear"></div>
	</div>
</div>
<div class="main">
	<div class="project-wrapper">
		<div class="wrap">
			<div class="section group">
				<?php
					$getAboutPhoto = $about->getAboutPhoto()->fetch_object();
					$getAboutDescription = $about->getAboutDescription()->fetch_object();
					echo "
					<div class='lsidebar span_1_of_about'>
						<img src='$getAboutPhoto->value' alt='' />
					</div>
					<div class='cont span_2_of_about'>
						<h2 style='font-weight: 600'>Welcome to our Team</h2>
						<h3 style='font-weight: 100 !important;'>$getAboutDescription->value</h3>
					</div>
					";					
				?>
				<div class="clear"></div>
			</div>
			<!-- <div class="about-middle">
				<h4><span>Our Skills</span></h4>
				<p>YOU CAN CHANGE THIS SECTION ONCE.</p>
			</div>
			<div class="section group example">
				<?php
					$no = 0;
					$skillLeft = "";
					$skillRight = "";
					$getAboutSkill = $about->getAboutSkill();
					while($data = $getAboutSkill->fetch_object()){
						$precentage = 50;
						$style = '';
						if ($data->precentage == 100){
							$style = 'background: #828282 !important;';
						} else {
							$precentage = (($data->precentage/100)*100)-31.5;							
						}
						if ($no & 1) {
							$skillLeft .= "
							<li style='$style'>
								<div class='percentage'>$data->precentage%</div>
								<div class='percent-text' style='width: $precentage% !important;'>$data->skill</div>
								<div class='clear'></div>
							</li>
							";
						} else {
							$skillRight .= "
							<li style='$style'>
								<div class='percentage'>$data->precentage%</div>
								<div class='percent-text' style='width: $precentage% !important;'>$data->skill</div>
								<div class='clear'></div>
							</li>
							";
						}
						$no++;
					}
				?>
				<div class="col_1_of_2 span_1_of_2" style="margin-bottom: 0px !important;padding-bottom: 0px !important;">
					<div class="skills">
						<ul>
							<?php echo $skillLeft; ?>
							<div class="clear"></div>
						</ul>
					</div>
				</div>
				<div class="col_1_of_2 span_1_of_2" style="margin-top: 0px !important;padding-top: 0px !important;">
					<div class="skills">
						<ul>
							<?php echo $skillRight; ?>
							<div class="clear"></div>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div> -->
			<div class="about-middle">
				<h4><span>Our Team</span></h4>
				<p><br></p>
			</div>
			<div class="team1">
			<?php
				$no=0;
				$getAboutTeam = $about->getAboutTeam();
				while($data = $getAboutTeam->fetch_object()){
					if($no == 4){
						echo "
						<div class='clear'></div>
						</div>
						<div class='team1'>";
					}
				?>
				<div class="col_1_of_about-grids span_1_of_about-grids" style="text-align: center;">
					<a class="popup-with-zoom-anim" href="#small-dialog<?php echo $data->id; ?>">
						<img src="<?php echo $data->image; ?>" alt="" />
						<h3><?php echo $data->title; ?></h3>
					</a>
					<!-- start magnific -->
					<div class="team-dialog mfp-hide" id="small-dialog<?php echo $data->id; ?>" class="mfp-hide">
						<div class="pop_up">
							<h2>about <?php echo $data->title; ?></h2>
							<p><?php echo $data->description; ?></p>
							<div class="social-icons">
								<h2 style="margin-top: 4%;">get in touch</h2>
								<ul>
									<?php
										$socialMedia = explode(',', $data->social_media);
									?>
									<li class="facebook"><a href="<?php echo @$socialMedia[0];?>" target="_blank"><span> </span></a></li>
									<li class="twitter"><a href="<?php echo @$socialMedia[1];?>" target="_blank"><span> </span></a></li>
									<li class="linkedin"><a href="<?php echo @$socialMedia[2];?>" target="_blank"><span> </span></a></li>
									<li class="youtube"><a href="<?php echo @$socialMedia[3];?>" target="_blank"><span> </span></a></li>
									<li class="instagram"><a href="<?php echo @$socialMedia[4];?>" target="_blank"><span> </span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- end  magnific -->
				</div>
				<?php
					$no++;
				}
			?>
			<div class="clear"></div>
			</div>
		</div>
	</div>
</div>