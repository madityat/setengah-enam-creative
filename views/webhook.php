<?php

	require_once('./../config/koneksi.php');
	require_once('./../config/database.php');

    $connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
	if (isset($_GET['portfolioLike'])) {
		require_once('./../models/portfolio.php');
		$portfolio = new Portfolio($connection);
		$like = $portfolio->portfolioLike($_GET['portfolioLike']);
		echo $like;
		die;
	}
	if (isset($_GET['blogIncrement'])) {
		require_once('./../models/blog.php');
		$blog = new Blog($connection);
		if ($_GET['blogIncrement'] == 'like') {
			$result = $blog->blogLike($_GET['id']);
		} else {
			$result = $blog->blogShare($_GET['blogIncrement'], $_GET['id']);
		}
		echo $result;
		die;
	}
?>