<?php
    require 'PHPMailer/src/Exception.php';
    require 'PHPMailer/src/PHPMailer.php';
    require 'PHPMailer/src/SMTP.php';
    
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

class Mail {
    
    protected $host = "lapras.rapidplex.com";
    protected $smtpauth = true;
    protected $email = 'contact@setengah6.com';
    protected $password = "reoyAI!K!DY%";
    protected $smtpsecure = "ssl";
    protected $port = 465;

    function __construct(){
    }
    public function info(){
        $info = array(
            "host" => $this->host,
            "smtpauth" => $this->smtpauth,
            "email" => $this->email,
            "password" => $this->password,
            "smtpsecure" => $this->smtpsecure,
            "port" => $this->port
        );
        echo json_encode($info);
    }
    public function sendMessage($penerima_email, $penerima_nama, $title, $subject, $message){
        $result = array(
            'status' => 0,
            'message' => '',
            'timestamp' => date('Y-m-d H:i:s')
        );
        try {
            $mail = new PHPMailer(true);
            //Server settings
            $mail->isSMTP();
            $mail->Host = $this->host;  //gmail SMTP server
            $mail->SMTPAuth = $this->smtpauth;
            $mail->Username = $this->email;   //username
            $mail->Password = $this->password;   //password
            $mail->SMTPSecure = $this->smtpsecure;
            $mail->Port = $this->port;  //SMTP port

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        
            //Recipients
            $mail->setFrom($this->email, $title);
            $mail->isHTML(true);    // Set email format to HTML
            $mail->Subject = $subject;
            $mail->addReplyTo($this->email, 'Information');
        
            $mail->addAddress($penerima_email, $penerima_nama);     // Add a recipient
        
            $mail->Body = $message;
        
            if ($mail->send()) {
                $result['status'] = 1;
                $result['message'] = 'Message has been sent to '.$penerima_email;
            } else {
                $result['message'] = 'Fail to send message to '.$penerima_email;
            }
        } catch (Exception $e) {
            $result['message'] = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";        
        }
        echo "<pre>";
        $result = json_encode($result);
        return json_decode($result);
    }
}
?>