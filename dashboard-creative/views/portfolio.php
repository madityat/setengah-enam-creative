<?php
    include "./../models/portfolio.php";
    $portfolio = new Portfolio($connection);
    $id = "";
    $tag = "";
    $title = "";
    $description = "";
    $image = "";
    if (isset($_POST['edit-data'])) {
        $getPortfolio = $portfolio->getPortfolio($_POST['id']);
        if ($getPortfolio) {
            $data = $getPortfolio->fetch_object();
            $id = $data->id;
            $description = $data->description;
            $tag = $data->tag;
            $title = $data->title;
            $image = $data->image;
        }
    }
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group col-md-12">
                <div class="col-md-2">
                    <label class="control-label" for="title">TAG <small>(Max : 50 character)</small></label>
                    <input type="text" name="tag" class="form-control" id="tag" value="<?php echo $tag;?>" maxlength="50" required>
                </div>
                <div class="col-md-10">
                    <label class="control-label" for="title">TITLE PORTFOLIO <small>(Max : 300 character)</small></label>
                    <input type="text" name="title" class="form-control" id="title" value="<?php echo $title;?>" maxlength="300" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="image">PORTFOLIO IMAGE URL <small>(Max : 10 image) (Use comma `,` to separate each images)</small></label>
                    <textarea name="image" class="form-control" id="image" required><?php echo $image;?></textarea>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="description">PORTFOLIO DESCRIPTION</label>
                    <textarea name="description" class="form-control" id="summernote"><?php echo $description;?></textarea>
                </div>
            </div>
            <!-- Button simpan -->
            <div class="col-md-12" style="text-align: right;">
                <div class="col-md-12">
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='EDIT & SAVE DATA'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD NEW PORTFOLIO'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
            if (isset($_POST['delete'])) {
                $stmt = $portfolio->delete('portfolio', $_POST['id']);
            }
            if (isset($_POST['add'])) {
                $stmt = $portfolio->addPortfolio($_POST);
            }
            if (isset($_POST['edit'])) {
                $stmt = $portfolio->editPortfolio($_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TAG</th>
                        <th>TITLE</th>
                        <th>LIKE</th>
                        <th>DESCRIPTION</th>
                        <th>PORTFOLIO IMAGE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getPortfolio = $portfolio->getPortfolio();
                if (!$getPortfolio) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getPortfolio->fetch_object()){
                        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
                        $actual_link = str_replace('dashboard.', '', $actual_link);
                ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->tag; ?></td>
                        <td><?php echo $data->title; ?></td>
                        <td><?php echo $data->like; ?></td>
                        <td>
                            <a href="<?php echo $actual_link;?>/portfolio-detail/portfolio/<?php echo $data->id; ?>" target="_blank"><button class="btn btn-info btn-xs">SEE DETAIL</button></a>
                        </td>
                        <td>
                        <?php
                            $image = explode(',', $data->image);
                            for ($i=0 ; $i<count($image) ; $i++) {
                                echo "<a href='$image[$i]' target='_blank'/>".$image[$i]."</a> , "; 
                            }
                        ?>
                        </td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>