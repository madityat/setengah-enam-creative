<div class="panel panel-default">
    <div class="panel-heading"><strong>Get Address Map Value</strong></div>
    <div class="panel-body" style="padding: 10px 0px;">
        <ol>
            <li>Open <a href="https://www.embedgooglemap.net/" target="_blank">https://www.embedgooglemap.net/</a></li>
            <li>
                Fill 'Enter Your Address' section <br>
                <img src="./assets/img/addressmap1.jpg" width="25%"/>
            </li>
            <li>
                Click 'Get HTML Code' button <br>
                <img src="./assets/img/addressmap2.jpg" width="25%"/>
            </li>
            <li>
                Copy all text inside textbox <br>
                <img src="./assets/img/addressmap3.jpg" width="25%"/>
            </li>
            <li>
                Go to menu `Website Setting`, then fill textbox with data type `ADDRESS MAP` <br>
                <img src="./assets/img/addressmap4.jpg" width="25%"/>
            </li>
            <li>After you save the data, it will be <strong>automatically parsed to short value</strong></li>
        </ol>
    </div>
</div>