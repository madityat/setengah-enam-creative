<?php
    if (isset($_POST['submit'])) {
        $target_dir = "../images/upload/";
        $uploadOk = 1;
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $target_file = $target_dir.date('Y-m-d-h-i-s').'-'.rand(0,9).'.'.$imageFileType;
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                echo "<div class='alert alert-danger'>
                        <strong>Fail!</strong> File is not an image.
                </div>";
                $uploadOk = 0;
            }
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "<div class='alert alert-danger'>
                    <strong>Fail!</strong> Sorry, your file is too large. Maximum size is 5MB
            </div>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "ico" ) {
            echo "<div class='alert alert-danger'>
                    <strong>Fail!</strong> Sorry, only JPG, JPEG, PNG, GIF & ICO files are allowed.
            </div>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<div class='alert alert-danger'>
                    <strong>Fail!</strong> Sorry, your file was not uploaded.
            </div>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "<div class='alert alert-success'>
                        <strong>Success!</strong> The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.
                </div>";
            } else {
                echo "<div class='alert alert-danger'>
                        <strong>Fail!</strong> Sorry, there was an error uploading your file.
                </div>";
            }
        }
    }
    if (isset($_POST['delete'])) {
        if (unlink($_POST['filename'])) {
            echo "<div class='alert alert-success'>
                    <strong>Success!</strong> The file ". $_POST['filename']. " has been deleted.
            </div>";
        } else {
            echo "<div class='alert alert-danger'>
                    <strong>Fail!</strong> Sorry, there was an error deleting your file.
            </div>";
        }
    }
?>
<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Upload Image Asset</strong></div>
        <form action="" method="post" enctype="multipart/form-data">            
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label" for="file">Select image to upload :</label>
                    <input type="file" class="form-control" name="fileToUpload" id="fileToUpload" required>
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Upload Image" class="btn btn-primary" name="submit">
            </div>
        </form>
    </div>
</div>
<?php
    foreach (glob("../images/upload/*") as $filename) {
        $fileEkstension = explode('.', $filename);
        if ($fileEkstension[count($fileEkstension)-1] != 'php'){
            //website url
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            $assetURL = $actual_link.str_replace('..', '', $filename);
            $assetURL = str_replace('dashboard.', '', $assetURL);
            ?>
                <div class="col-md-2">
                    <div class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <a href="<?php echo $assetURL; ?>" target="_blank">
                                <img src="<?php echo $assetURL; ?>" style="max-height:123px;width: 100%;height: auto;" />
                            </a>
                        </div>
                        <div class="panel-footer">
                            <form action="" method="post">
                                <input type="text" value="<?php echo $assetURL;?>" name="asset_url" style="width: 100%;" readonly />
                                <input type="hidden" value="<?php echo $filename;?>" name="filename" />
                                <input type="submit" class="btn btn-danger btn-xs" style="width:100%;" name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');">
                            </form>
                        </div>
                    </div>
                </div>
            <?php
        }
    }
?>
