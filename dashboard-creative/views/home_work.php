<?php
    include "./../models/home.php";
    $home = new Home($connection);
    
    $id = "";
    $type = "";
    $value = "";
    if (isset($_POST['edit-data'])) {
        $getWork = $home->getWork('id', $_POST['id']);
        if ($getWork) {
            $data = $getWork->fetch_object();
            $id = $data->id;
            $type = $data->type;
            $value = $data->value;
        }
    }
    if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
        if (isset($_POST['delete'])) {
            $stmt = $home->delete('home_work', $_POST['id']);
        }
        if (isset($_POST['add'])) {
            $stmt = $home->addWork($_POST);
        }
        if (isset($_POST['edit'])) {
            $stmt = $home->editWork($_POST);
        }
        $alert = 'alert alert-success';
        $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
        if (!$stmt) {
            $alert = 'alert alert-danger';
            $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
        }
        echo "
        <div class='".$alert."'>
            ".$message."
        </div>
        ";
    }
    $getWorkTitle = $home->getWork('type', 'title')->fetch_object();
    $idTitle = $getWorkTitle->id;
    $valueTitle = $getWorkTitle->value;
    $getWorkLimit = $home->getWork('type', 'limit')->fetch_object();
    $idLimit = $getWorkLimit->id;
    $valueLimit = $getWorkLimit->value;
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $idTitle;?>">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label" for="type">TYPE OF DATA</label>
                    <select type="text" name="type" class="form-control" id="type" readonly>
                        <option value="title">SECTION TITLE</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <label class="control-label" for="value">VALUE OF DATA <small>(Max : 200 character for 'SECTION TITLE' and 'PHOTO FOR WORK') ('LIMIT PHOTO TO SHOW' must be a number)</small></label>
                    <input type="text" name="value" class="form-control" id="value" value="<?php echo $valueTitle;?>" maxlength="200" required>
                </div>
                <div class="col-md-1">
                    <label class="control-label" for="title"><hr></label>
                    <input type='submit' class='btn btn-info' name='edit' value='SAVE'>
                </div>
            </div>
        </div>
    </form>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $idLimit;?>">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label" for="type">TYPE OF DATA</label>
                    <select type="text" name="type" class="form-control" id="type" readonly>
                        <option value="limit">LIMIT PHOTO TO SHOW</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <label class="control-label" for="value">VALUE OF DATA <small>(Max : 200 character for 'SECTION TITLE' and 'PHOTO FOR WORK') ('LIMIT PHOTO TO SHOW' must be a number)</small></label>
                    <input type="number" name="value" class="form-control" id="value" value="<?php echo $valueLimit;?>" maxlength="3" required>
                </div>
                <div class="col-md-1">
                    <label class="control-label" for="title"><hr></label>
                    <input type='submit' class='btn btn-info' name='edit' value='SAVE'>
                </div>
            </div>
        </div>
    </form>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label" for="type">TYPE OF DATA</label>
                    <select type="text" name="type" class="form-control" id="type" readonly>
                        <option value="photo">PHOTO OF WORK</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <label class="control-label" for="value">VALUE OF DATA <small>(Max : 200 character for 'SECTION TITLE' and 'PHOTO FOR WORK') ('LIMIT PHOTO TO SHOW' must be a number)</small></label>
                    <input type="text" name="value" class="form-control" id="value" value="<?php echo $value;?>" maxlength="200" required>
                </div>
                <div class="col-md-1">
                    <label class="control-label" for="title"><hr></label>
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='SAVE'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TYPE OF DATA</th>
                        <th>VALUE OF DATA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getWork = $home->getWork('type','photo');
                if (!$getWork) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getWork->fetch_object()){
                ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->type; ?></td>
                        <td><?php echo $data->value; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
