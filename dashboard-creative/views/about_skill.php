<?php
    include "./../models/about.php";
    $about = new About($connection);
    $id = "";
    $skill = "";
    $precentage = "";
    if (isset($_POST['edit-data'])) {
        $getAboutSkill = $about->getAboutSkill($_POST['id']);
        if ($getAboutSkill) {
            $data = $getAboutSkill->fetch_object();
            $id = $data->id;
            $skill = $data->skill;
            $precentage = $data->precentage;
        }
    }
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="control-label" for="skill">SKILL NAME <small>(Max : 50 character)</small></label>
                    <input type="text" name="skill" class="form-control" id="skill" value="<?php echo $skill;?>" maxlength="50" required>
                </div>
                <div class="col-md-3">
                    <label class="control-label" for="precentage">SKILL PRECENTAGE <small>(0-100%)</small></label>
                    <input type="number" name="precentage" class="form-control" id="precentage" value="<?php echo $precentage;?>" min="0" max="100" required>
                </div>
                <div class="col-md-2" style="text-align: right;">
                    <label class="control-label" for="button"> <small>Action:</small></label>
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='EDIT & SAVE DATA'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD NEW SKILL'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
            if (isset($_POST['delete'])) {
                $stmt = $about->delete('about_skill', $_POST['id']);
            }
            if (isset($_POST['add'])) {
                $stmt = $about->addSkill($_POST);
            }
            if (isset($_POST['edit'])) {
                $stmt = $about->editSkill($_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SKILL NAME</th>
                        <th>PRECENTAGE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getAboutSkill = $about->getAboutSkill();
                if (!$getAboutSkill) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getAboutSkill->fetch_object()){
            ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->skill; ?></td>
                        <td><?php echo $data->precentage; ?>%</td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
