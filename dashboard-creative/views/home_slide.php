<?php
    include "./../models/home.php";
    $home = new Home($connection);
    $id = "";
    $title_1 = "";
    $title_2 = "";
    $image = "";
    $sub_title_1 = "";
    $sub_title_2 = "";
    $sub_title_3 = "";
    if (isset($_POST['edit-data'])) {
        $getSlide = $home->getSlide($_POST['id']);
        if ($getSlide) {
            $data = $getSlide->fetch_object();
            $id = $data->id;
            $title_1 = $data->title_1;
            $title_2 = $data->title_2;
            $image = $data->image;
            $sub_title_1 = $data->sub_title_1;
            $sub_title_2 = $data->sub_title_2;
            $sub_title_3 = $data->sub_title_3;
        }
    }
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group col-md-12">
                <div class="col-md-4">
                    <label class="control-label" for="title_1">TITLE 1 <small>(Max : 10 character)</small></label>
                    <input type="text" name="title_1" class="form-control" id="title_1" value="<?php echo $title_1;?>" maxlength="10" required>
                </div>
                <div class="col-md-4">
                <label class="control-label" for="title_2">TITLE 2 <small>(Max : 26 character)</small></label>
                    <input type="text" name="title_2" class="form-control" id="title_2" value="<?php echo $title_2;?>" maxlength="25" required>
                </div>
                <div class="col-md-4">
                    <label class="control-label" for="image">SLIDE IMAGE URL <small>(Max : 100 character)</small></label>
                    <input type="text" name="image" class="form-control" id="image" value="<?php echo $image;?>" maxlength="100" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-4">
                    <label class="control-label" for="sub_title_1">SUB TITLE 1 <small>(Max : 30 character)</small></label>
                    <input type="text" name="sub_title_1" class="form-control" id="sub_title_1" value="<?php echo $sub_title_1;?>" maxlength="30" required>
                </div>
                <div class="col-md-4">
                    <label class="control-label" for="sub_title_2">SUB TITLE 2 <small>(Max : 30 character)</small></label>
                    <input type="text" name="sub_title_2" class="form-control" id="sub_title_2" value="<?php echo $sub_title_2;?>" maxlength="30" required>
                </div>
                <div class="col-md-4">
                    <label class="control-label" for="sub_title_3">SUB TITLE 3 <small>(Max : 25 character)</small></label>
                    <input type="text" name="sub_title_3" class="form-control" id="sub_title_3" value="<?php echo $sub_title_3;?>" maxlength="25" required>
                </div>
            </div>
            <!-- Button simpan -->
            <div class="col-md-12" style="text-align: right;">
                <div class="col-md-12">
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='EDIT & SAVE DATA'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD NEW SLIDE'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
            if (isset($_POST['delete'])) {
                $stmt = $home->delete('home_slide', $_POST['id']);
            }
            if (isset($_POST['add'])) {
                $stmt = $home->addSlide($_POST);
            }
            if (isset($_POST['edit'])) {
                $stmt = $home->editSlide($_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TITLE 1</th>
                        <th>TITLE 2</th>
                        <th>SUB TITLE 1</th>
                        <th>SUB TITLE 2</th>
                        <th>SUB TITLE 3</th>
                        <th>SLIDE IMAGE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getSlide = $home->getSlide();
                if (!$getSlide) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getSlide->fetch_object()){
                ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->title_1; ?></td>
                        <td><?php echo $data->title_2; ?></td>
                        <td><?php echo $data->sub_title_1; ?></td>
                        <td><?php echo $data->sub_title_2; ?></td>
                        <td><?php echo $data->sub_title_3; ?></td>
                        <td><?php echo "<a href='$data->image' target='_blank'/>".$data->image."</a>"; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
