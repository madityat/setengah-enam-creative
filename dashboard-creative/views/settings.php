<?php
    $id = "";
    $type = "";
    $value = "";
    if (isset($_POST['edit'])) {
        $stmt = $setting->editSetting($_POST);
        $alert = 'alert alert-success';
        $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
        if (!$stmt) {
            $alert = 'alert alert-danger';
            $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
        }
        echo "
        <div class='".$alert."'>
            ".$message."
        </div>
        ";
    }
    $getSettings = $setting->getSettings();
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <?php
    if (!$getSettings) {
            echo "<center><h4>Tidak Dapat Menampilkan Data</h4></center>";
        } else {
            while($data = $getSettings->fetch_object()){
            ?>
            <form method="post" enctype="multipart/form-data">
                <div class="panel-body" style="padding: 10px 0px;">
                    <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $data->id;?>">
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="control-label" for="type">TYPE OF DATA</label>
                            <select type="text" name="type" class="form-control" id="type" readonly>
                                <option value="<?php echo $data->type;?>"><?php echo strtoupper($data->type);?></option>
                            </select>
                            <?php
                                if($data->type === 'address_map') {
                                    echo "<a href='?page=how_to' target='_blank'>See how to get address map</a>";
                                }
                            ?>
                        </div>
                        <div class="col-md-8">
                            <label class="control-label" for="value">VALUE OF DATA <small>
                            <?php
                                if($data->type === 'address_map') {
                                    echo "<a href='?page=how_to' target='_blank' style='color:red;'>See how to get address map</a>";
                                }
                            ?>
                            </small></label>
                            <textarea type="text" name="value" class="form-control" id="value"><?php echo $data->value;?></textarea>
                        </div>
                        <div class="col-md-1">
                            <label class="control-label" for="title"><hr></label>
                            <input type='submit' class='btn btn-info' name='edit' value='SAVE'>
                        </div>
                    </div>
                </div>
            </form>
            <?php
            }
        }
    ?>
</div>
