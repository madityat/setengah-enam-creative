<?php
    include "./../models/subscribe.php";
    $subscribe = new Subscribe($connection);
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="subject">SUBJECT EMAIL <small>(Max : 300 character)</small></label>
                    <input type="text" name="subject" class="form-control" id="subject" maxlength="300" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="body">BODY EMAIL</label>
                    <textarea name="body" class="form-control" id="summernote"></textarea>
                </div>
            </div>
            <!-- Button simpan -->
            <div class="col-md-12" style="text-align: right;">
                <div class="col-md-12">
                    <input type='submit' class='btn btn-success' name='send' value='START BROADCAST EMAIL'>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['send'])) {
            if (isset($_POST['delete'])) {
                $stmt = $subscribe->delete('subscribe', $_POST['id']);
            }
            if (isset($_POST['send'])) {
                $stmt = $subscribe->sendEmail('all', $_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>EMAIL</th>
                        <th>SUBSCRIBE DATE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getSubscriber = $subscribe->getSubscriber();
                if (!$getSubscriber) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getSubscriber->fetch_object()){
                ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->email; ?></td>
                        <td><?php echo date('d F Y, H:i:s', strtotime($data->createdAt)); ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    $no++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>