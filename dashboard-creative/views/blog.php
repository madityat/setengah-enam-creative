<?php
    include "./../models/blog.php";
    $blog = new Blog($connection);
    $id = "";
    $category = "";
    $author = "";
    $title = "";
    $description = "";
    $image = "";
    if (isset($_POST['edit-data'])) {
        $getBlog = $blog->getBlog($_POST['id']);
        if ($getBlog) {
            $data = $getBlog->fetch_object();
            $id = $data->id;
            $description = $data->description;
            $category = $data->category;
            $author = $data->author;
            $title = $data->title;
            $image = $data->image;
        }
    }
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group col-md-12">
                <div class="col-md-3">
                    <label class="control-label" for="title">CATEGORY <small>(Max : 50 character)</small></label>
                    <input type="text" name="category" class="form-control" id="category" value="<?php echo $category;?>" maxlength="50" required>
                </div>
                <div class="col-md-9">
                    <label class="control-label" for="title">AUTHOR NAME <small>(Max : 250 character)</small></label>
                    <input type="text" name="author" class="form-control" id="author" value="<?php echo $author;?>" maxlength="250" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="title">TITLE BLOG <small>(Max : 300 character)</small></label>
                    <input type="text" name="title" class="form-control" id="title" value="<?php echo $title;?>" maxlength="300" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="image">BLOG BANNER IMAGE URL <small>(Only 1 image)</small></label>
                    <input type="text" name="image" class="form-control" id="image" value="<?php echo $image;?>" maxlength="300" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="description">BLOG DESCRIPTION</label>
                    <textarea name="description" class="form-control" id="summernote" required><?php echo $description;?></textarea>
                </div>
            </div>
            <!-- Button simpan -->
            <div class="col-md-12" style="text-align: right;">
                <div class="col-md-12">
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='EDIT & SAVE DATA'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD NEW BLOG'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
            if (isset($_POST['delete'])) {
                $stmt = $blog->delete('blog', $_POST['id']);
            }
            if (isset($_POST['add'])) {
                $stmt = $blog->addBlog($_POST);
            }
            if (isset($_POST['edit'])) {
                $stmt = $blog->editBlog($_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>CATEGORY</th>
                        <th>AUTHOR</th>
                        <th>TITLE</th>
                        <th>LIKE</th>
                        <th>SHARE</th>
                        <th>DESCRIPTION</th>
                        <th>BLOG IMAGE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getBlog = $blog->getBlog();
                if (!$getBlog) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
                    $actual_link = str_replace('dashboard.', '', $actual_link);
                    while($data = $getBlog->fetch_object()){
                ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->category; ?></td>
                        <td><?php echo $data->author; ?></td>
                        <td><?php echo $data->title; ?></td>
                        <td><?php echo $data->like; ?></td>
                        <td><?php
                            $totalShare = $data->facebook+$data->twitter+$data->linkedin;
                            echo $totalShare;
                        ?></td>
                        <td>
                            <a href="<?php echo $actual_link;?>/blog-detail/blog/<?php echo $data->id; ?>" target="_blank"><button class="btn btn-info btn-xs">SEE DETAIL</button></a>
                        </td>
                        <td>
                            <a href='<?php echo $data->image;?>' target='_blank'><?php echo $data->image;?></a>
                        </td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>                        
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>