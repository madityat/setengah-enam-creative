<?php
    include "./../models/about.php";
    $about = new About($connection);
    $id = "";
    $description = "";
    $title = "";
    $image = "";
    $social_media = explode(',', "https://facebook.com,https://twitter.com,https://linkedin.com,https://youtube.com,https://instagram.com");;
    if (isset($_POST['edit-data'])) {
        $getAboutTeam = $about->getAboutTeam($_POST['id']);
        if ($getAboutTeam) {
            $data = $getAboutTeam->fetch_object();
            $id = $data->id;
            $description = $data->description;
            $title = $data->title;
            $image = $data->image;
            $social_media = explode(',', $data->social_media);
        }
    }
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="control-label" for="title">TITLE <small>(Max : 50 character)</small></label>
                    <input type="text" name="title" class="form-control" id="title" value="<?php echo $title;?>" maxlength="50" required>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="image">PHOTO PROFILE URL <small>(Max : 100 character)</small></label>
                    <input type="text" name="image" class="form-control" id="image" value="<?php echo $image;?>" maxlength="100" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="description">SHORT DESCRIPTION <small>(Max : 500 character)</small></label>
                    <textarea name="description" class="form-control" id="description" maxlength="500" required><?php echo $description;?></textarea>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-4">
                    <label class="control-label" for="facebook">FACEBOOK <small>(Max : 100 character)</small></label>
                    <input type="text" name="facebook" class="form-control" id="facebook" value="<?php echo $social_media[0];?>" maxlength="50" required>
                </div>
                <div class="col-md-4">
                    <label class="control-label" for="twitter">TWITTER <small>(Max : 100 character)</small></label>
                    <input type="text" name="twitter" class="form-control" id="twitter" value="<?php echo $social_media[1];?>" maxlength="100" required>
                </div>
                <div class="col-md-4">
                    <label class="control-label" for="linkedin">LINKEDIN <small>(Max : 100 character)</small></label>
                    <input type="text" name="linkedin" class="form-control" id="linkedin" value="<?php echo $social_media[2];?>" maxlength="100" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-4">
                    <label class="control-label" for="youtube">YOUTUBE <small>(Max : 100 character)</small></label>
                    <input type="text" name="youtube" class="form-control" id="youtube" value="<?php echo $social_media[3];?>" maxlength="50" required>
                </div>
                <div class="col-md-4">
                    <label class="control-label" for="instagram">INSTAGRAM <small>(Max : 100 character)</small></label>
                    <input type="text" name="instagram" class="form-control" id="instagram" value="<?php echo $social_media[4];?>" maxlength="100" required>
                </div>
            </div>
            <!-- Button simpan -->
            <div class="col-md-12" style="text-align: right;">
                <div class="col-md-12">
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='EDIT & SAVE DATA'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD NEW TEAM'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
            if (isset($_POST['delete'])) {
                $stmt = $about->delete('about_team', $_POST['id']);
            }
            if (isset($_POST['add'])) {
                $_POST['social_media'] = $_POST['facebook'].",".$_POST['twitter'].",".$_POST['linkedin'].",".$_POST['youtube'].",".$_POST['instagram'];
                $stmt = $about->addTeam($_POST);
            }
            if (isset($_POST['edit'])) {
                $_POST['social_media'] = $_POST['facebook'].",".$_POST['twitter'].",".$_POST['linkedin'].",".$_POST['youtube'].",".$_POST['instagram'];
                $stmt = $about->editTeam($_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>PHOTO PROFILE</th>
                        <th>SOCIAL MEDIA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getAboutTeam = $about->getAboutTeam();
                if (!$getAboutTeam) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getAboutTeam->fetch_object()){
            ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->title; ?></td>
                        <td><?php echo $data->description; ?></td>
                        <td><?php echo "<a href='$data->image' target='_blank'/>".$data->image."</a>"; ?></td>
                        <td>
                        <?php
                            $socialMedia = explode(',', $data->social_media);
                            for($i=0 ; $i<count($socialMedia) ; $i++){
                                echo $socialMedia[$i]."<br>";
                            }
                        ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
