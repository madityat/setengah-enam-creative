<ul class="nav navbar-nav side-nav">
    <li><a href="?page=dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-home"></i> Manage Home Page <b class="caret"></b></a>
        <ul class="dropdown-menu">
        <li><a href="?page=home_slide">Slide Section</a></li>
        <li><a href="?page=home_service">Service Section</a></li>
        <li><a href="?page=home_work">Our Work Section</a></li>
        <li><a href="?page=home_client">Our Client Section</a></li>
        </ul>
    </li>
    <li><a href="?page=portfolio"><i class="fa fa-file"></i> Manage Portfolio </a></li>
    <li><a href="?page=blog"><i class="fa fa-book"></i> Manage Blog </a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-home"></i> Manage About Page <b class="caret"></b></a>
        <ul class="dropdown-menu">
        <li><a href="?page=about_description">Description Section</a></li>
        <!-- <li><a href="?page=about_skill">Skill Section</a></li> -->
        <li><a href="?page=about_team">Team Section</a></li>
        </ul>
    </li>
    <li><a href="?page=asset_image_library"><i class="fa fa-cloud-upload"></i> Asset Image Library </a></li>
    <li><a href="?page=subscribe"><i class="fa fa-users"></i> Manage Subscriber </a></li>
    <li><a href="?page=setting"><i class="fa fa-cogs"></i> Website Setting </a></li>
</ul>
