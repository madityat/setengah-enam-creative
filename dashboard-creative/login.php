<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dashboard Management Content</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="asset_login/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="asset_login/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="asset_login/css/form-elements.css">
        <link rel="stylesheet" href="asset_login/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="asset_login/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="asset_login/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="asset_login/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="asset_login/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="asset_login/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>DASHBOARD MANAGEMENT CONTENT</strong></h1>
                        </div>
                    </div>
                    <?php
                        if (isset($_POST['login'])) {	// MENGECEK JIKA BUTTON LOGIN DITEKAN
							if ($_POST['username'] == 'set6creative' && $_POST['password'] == 'set6creative') {	// JIKA USERNAME DAN PASSWORD = ADMIN
								$_SESSION['login'] = true;	// SET LOGIN = TRUE AGAR TAHU USER SUDAH LOGIN
								$_SESSION['login_as'] = 'admin';	// SET LOGIN SEBAGAI ADMIN
								header("Location: ./?page=dashboard");
								die();
							}
						}
					?>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                            		<p style="font-weight: bold;">Dashboard Management Content is used to change and manage website content or website setting.</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" name="form-login" action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="username">Username</label>
			                        	<input type="text" name="username" placeholder="Username..." class="form-username form-control" id="username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control">
			                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <button type="submit" name="login" class="btn">LOGIN</button>
                                        </div>
                                    </div>
                                </form>                            
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="asset_login/js/jquery-1.11.1.min.js"></script>
        <script src="asset_login/bootstrap/js/bootstrap.min.js"></script>
        <script src="asset_login/js/jquery.backstretch.min.js"></script>
        <script src="asset_login/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="asset_login/js/placeholder.js"></script>
        <![endif]-->
        
    </body>
</html>