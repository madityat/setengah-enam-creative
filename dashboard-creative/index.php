<?php
  session_start();
  if (!isset($_SESSION['login'])) {
    header("Location: ./login.php");
    die();
  }

  require_once('./../config/koneksi.php');
  require_once('./../config/database.php');
  require_once('./../models/setting.php');

  $connection = new Database($host, $user, $pass, $database);
  $setting = new Setting($connection);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo $setting->getSettingData('type', 'logo')->value; ?>">

    <title>Dashboard Management Content</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

    <!-- JavaScript -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/bootstrap.js"></script>

    <!-- include summernote css/js -->
    <link href="assets/summernote/summernote.css" rel="stylesheet">
    <script src="assets/summernote/summernote.js"></script>

    <!-- include datatable css/js -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

  </head>
  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">Dashboard Management Content</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <?php
            if ($_SESSION['login_as'] === 'admin') {
              include 'menu_admin.php';
            } else {
              header("Location: ./login.php");
              die();
            }
          ?>
          <ul class="nav navbar-nav navbar-right navbar-user">          
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo ucfirst($_SESSION['login_as']); ?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="./logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h1>
              <?php
                $page = 'Dashboard';
                if (isset($_GET['page'])) {
                  $page = str_replace('_',' ', ucwords($_GET['page'], '_')); 
                }
                echo $page;
              ?>
            </h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Dashboard</a></li>
              <li class="active"><i class="icon-file-alt"></i><?php echo $page; ?></li>
            </ol>
          </div>
        </div><!-- /.row -->

        <?php
        if(@$_GET['page'] == 'dashboard' || @$_GET['page'] == ''){
          include "views/dashboard.php";
        } else if (@$_GET['page'] == 'asset_image_library'){
          include "views/asset_upload.php";
        } else if (@$_GET['page'] == 'home_slide'){
          include "views/home_slide.php";
        } else if (@$_GET['page'] == 'home_service'){
          include "views/home_service.php";
        } else if (@$_GET['page'] == 'home_work'){
          include "views/home_work.php";
        } else if (@$_GET['page'] == 'home_client'){
          include "views/home_client.php";
        } else if (@$_GET['page'] == 'portfolio'){
          include "views/portfolio.php";
        } else if (@$_GET['page'] == 'blog'){
          include "views/blog.php";
        } else if (@$_GET['page'] == 'setting'){
          include "views/settings.php";
        } else if (@$_GET['page'] == 'how_to'){
          include "views/howto.php";
        } else if (@$_GET['page'] == 'about_description'){
          include "views/about_description.php";
        } else if (@$_GET['page'] == 'about_skill'){
          include "views/about_skill.php";
        } else if (@$_GET['page'] == 'about_team'){
          include "views/about_team.php";
        } else if (@$_GET['page'] == 'subscribe'){
          include "views/subscribe.php";
        }
        ?>
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    <style>
      .table-resposive {
        margin-bottom: 5% !important;
      }
    </style>
    <script>
      $("input").keypress(function(e){
        if (e.target.value.length == e.target.maxLength) {
          alert('Maksimal panjang karakter untuk data ini adalah '+e.target.maxLength);
        }
      });
      $(document).ready(function() {
          $('#summernote').summernote();
      });
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script>
  </body>
</html>