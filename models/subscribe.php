<?php
class Subscribe {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function getSubscriber($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_subscribe";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY email ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function addSubscriber($data){
        $db = $this->mysqli->conn;
        $sql = "REPLACE INTO `tbl_subscribe` (`email`) VALUES ('".$data['email']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function delete($table, $id){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_$table WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function sendEmail($email, $data){
        $db = $this->mysqli->conn;
        $value = "";
        if ($email == 'all') {
            $getSubscriber = $this->getSubscriber();
            $total = $getSubscriber->num_rows;
            for($i=1 ; $i<=$total ; $i++){
                $subscriber = $getSubscriber->fetch_object();
                $value .= "('".$subscriber->email."', '".$data['subject']."', '".$data['body']."')";
                if ($i!=$total) {
                    $value .= ",";
                }
            }
        } else {
            $value .= "('".$email."', '".$data['subject']."', '".$data['body']."')";
        }
        $sql = "INSERT INTO `tbl_email_notification` (`email`, `subject`, `body`) VALUES $value;";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>