<?php
class Home {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function getSlide($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_home_slide";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getService($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_home_service";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getWork($column = null, $value = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_home_work";
        if($column != null){
            $sql .= " WHERE $column = '$value'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getClient($column = null, $value = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_home_client";
        if($column != null){
            $sql .= " WHERE $column = '$value'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function delete($table, $id){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_$table WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addSlide($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_home_slide` (`title_1`, `title_2`, `sub_title_1`, `sub_title_2`, `sub_title_3`, `image`) ";
        $sql .= "VALUES('".$data['title_1']."','".$data['title_2']."','".$data['sub_title_1']."','".$data['sub_title_2']."','".$data['sub_title_3']."','".$data['image']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editSlide($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_home_slide SET title_1 = '".$data['title_1']."', title_2 = '".$data['title_2']."', sub_title_1 = '".$data['sub_title_1']."', ";
        $sql .= "sub_title_2 = '".$data['sub_title_2']."', sub_title_3 = '".$data['sub_title_3']."', image = '".$data['image']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addService($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_home_service` (`title`, `description`, `image`) ";
        $sql .= "VALUES('".$data['title']."','".$data['description']."','".$data['image']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editService($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_home_service SET title = '".$data['title']."', `description` = '".$data['description']."', image = '".$data['image']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addWork($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_home_work` (`type`, `value`) ";
        $sql .= "VALUES('".$data['type']."','".$data['value']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editWork($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_home_work SET type = '".$data['type']."', `value` = '".$data['value']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addClient($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_home_client` (`type`, `value`) ";
        $sql .= "VALUES('".$data['type']."','".$data['value']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editClient($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_home_client SET type = '".$data['type']."', `value` = '".$data['value']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
}
?>