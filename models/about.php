<?php
class About {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function getAbout(){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_about_description";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editAboutDescription($data = null){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_about_description SET `value` = '".$data['value']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function getAboutPhoto(){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_about_description WHERE `type` = 'photo'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getAboutDescription(){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_about_description WHERE `type` = 'description'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getAboutSkill($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_about_skill";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getAboutTeam($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_about_team";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function addTeam($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_about_team` (`title`, `description`, `image`, `social_media`) ";
        $sql .= "VALUES('".$data['title']."','".$data['description']."','".$data['image']."','".$data['social_media']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editTeam($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_about_team SET title = '".$data['title']."', `description` = '".$data['description']."', `image` = '".$data['image']."', social_media = '".$data['social_media']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addSkill($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_about_skill` (`skill`, `precentage`) ";
        $sql .= "VALUES('".$data['skill']."','".$data['precentage']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editSkill($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_about_skill SET skill = '".$data['skill']."', `precentage` = '".$data['precentage']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function delete($table, $id){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_$table WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
}
?>