<?php
class Setting {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function getSettings(){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_setting ORDER BY type ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getSettingData($column = null, $value = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_setting";
        if($column != null){
            $sql .= " WHERE $column = '$value'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        if ($query) {
            return $query->fetch_object();
        }
        return $query;
    }
    public function editSetting($data = null){
        $db = $this->mysqli->conn;
        if ($data['type'] === 'address_map') {
            $data['value'] = $this->extractAddressMap($data['value']);
        }
        $sql = "UPDATE tbl_setting SET `value` = '".$data['value']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function extractAddressMap($raw) {
        $raw = explode('src="', $raw);
        $raw = explode('"', $raw[1]);
        return $raw[0];
    }
}
?>