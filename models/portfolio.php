<?php
class Portfolio {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function getPortfolioTag($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT id, tag FROM tbl_portfolio";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " GROUP BY tag ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getPortfolio($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_portfolio";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getPortfolioLimit($limit = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_portfolio ORDER BY createdAt DESC";
        if($limit != null){
            $sql .= " LIMIT 0, $limit";
        }
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getPortfolioPopular($limit = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_portfolio ORDER BY `like` DESC";
        if($limit != null){
            $sql .= " LIMIT 0, $limit";
        }
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function delete($table, $id){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_$table WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addPortfolio($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_portfolio` (`tag`, `title`, `description`, `image`, `like`) ";
        $sql .= "VALUES('".$data['tag']."','".$data['title']."','".$data['description']."','".$data['image']."',0);";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editPortfolio($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_portfolio SET tag = '".$data['tag']."', title = '".$data['title']."', description = '".$data['description']."', ";
        $sql .= "image = '".$data['image']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function portfolioLike($id){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_portfolio SET `like` = `like`+1 WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
}
?>