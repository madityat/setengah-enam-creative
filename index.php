<?php
    session_start();
    require_once('./config/koneksi.php');
    require_once('./config/database.php');
    require_once('./models/setting.php');
    require_once('./models/about.php');
    require_once('./models/subscribe.php');

    $connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
    $setting = new Setting($connection);
	$about = new About($connection);
	$subscribe = new Subscribe($connection);
?>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>

<head>
    <base href="<?php echo ($_SERVER['HTTP_HOST'] === 'setengah6.com') ? '/':'/projek/setengah6/'; ?>" />
    <title>Setengah Enam Creative</title>
    <link rel="icon" href="<?php echo $setting->getSettingData('type', 'logo')->value; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='https://fonts.googleapis.com/css?family=Karla' rel='stylesheet' type='text/css'>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <style>
        .index-banner{
            background:url(<?php echo $setting->getSettingData('type', 'banner')->value; ?>)no-repeat !important;
            padding:10px 0;
        }
        .banner{
            background:url(<?php echo $setting->getSettingData('type', 'banner')->value; ?>)no-repeat !important;
            padding:50px 0;
        }
        .no_link { 
            cursor: pointer; 
        }        
        .team-dialog{
            background: white;
            padding: 20px 30px;
            text-align: left;
            max-width: 400px;
            margin: 40px auto;
            position: relative;
            border-radius:2px;
            -webkit-border-radius:2px;
            -moz-border-radius:2px;
            -o-border-radius:2px;
        }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript">
	$(function () {
		var filterList = {
			init: function () {
				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixitup({
					targetSelector: '.portfolio',
					filterSelector: '.filter',
					effects: ['fade'],
					easing: 'snap',
					// call the hover effect
					onMixEnd: filterList.hoverEffect()
				});				
			
			},
			hoverEffect: function () {
				// Simple parallax effect
				$('#portfoliolist .portfolio').hover(
					function () {
						$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
						$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
					},
					function () {
						$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
						$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
					}		
				);				
			}
		};
		// Run the show!
		filterList.init();
	});	
	</script>
    <!-- Add fancyBox main JS and CSS files -->
    <script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
    <link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
    <script>
        $(document).ready(function() {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
        });
        function portfolioLike(id) {
            var likeId = document.getElementById('like'+id);
            $.get('./views/webhook.php?portfolioLike='+id, function(data, status){
                if (status === 'success') {
                    if(data == 1) {
                        likeId.innerHTML = parseInt(likeId.innerHTML)+1;
                    } else {
                        alert ('Gagal menambah like.');
                    }
                }
            });
        }
        function blogIncrement(type, id) {
            var element = document.getElementById(type+id);
            $.get('./views/webhook.php?blogIncrement='+type+'&id='+id, function(data, status){
                if (status === 'success') {
                    if(data == 1) {
                        element.innerHTML = parseInt(element.innerHTML)+1;
                    } else {
                        alert ('Gagal menambah nilai.');
                    }
                }
            });
        }
    </script>
</head>

<body>
    <div class="header">
        <div class="wrap">
            <div class="logo">
                <a href="./home"><img src="<?php echo $setting->getSettingData('type', 'logo')->value; ?>" alt="" /></a>
            </div>
            <div class="cssmenu">
                <ul>
                    <li class="<?php echo (@$_GET['page'] == 'home') ? 'active' : '';?>"><a href="./home">Home</a></li>
                    <li class="<?php echo (@$_GET['page'] == 'portfolio') ? 'active' : '';?>"><a href="./portfolio">Portfolio</a></li>
                    <li class="<?php echo (@$_GET['page'] == 'blog') ? 'active' : '';?>"><a href="./blog">Blog</a></li>
                    <li class="<?php echo (@$_GET['page'] == 'about') ? 'active' : '';?>"><a href="./about">About Us</a></li>
                    <li class="<?php echo (@$_GET['page'] == 'contact') ? 'active' : '';?>"><a href="./contact">Contact</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	<?php
		if(isset($_GET['page'])) {
			$page = $_GET['page'];
			switch($page) {
				case 'home' : include_once('./views/home.php'); break;
				case 'portfolio' : include_once('./views/portfolio.php'); break;
				case 'blog' : include_once('./views/blog.php'); break;
				case 'blog-detail' : include_once('./views/blog.php'); break;
				case 'about' : include_once('./views/about.php'); break;
				case 'contact' : include_once('./views/contact.php'); break;
				case 'portfolio-detail' : include_once('./views/portfolio_detail.php'); break;
				default : include_once('./views/404.php');
			}
		} else {
			include_once('./views/home.php');
		}
	?>
    <div class="footer">
        <div class="footer-top">
            <div class="wrap">
                <div class="section group">
                    <div class="col_1_of_3 span_1_of_3">
                        <h3>About Us</h3>
                        <?php
                            $getAboutDescription = $about->getAboutDescription()->fetch_object();
                            $getAboutDescription = $getAboutDescription->value;
                            if (strlen($getAboutDescription) > 325) {
                                $getAboutDescription = substr($getAboutDescription, 0, 315)." . . .";
                            }
                            echo "<p>$getAboutDescription</p>"
                        ?>                        
                        <a href="./about"><button class="btn1 btn-8 btn-8b">Learn more</button></a>
                    </div>
                    <div class="col_1_of_3 span_1_of_3">
                        <h3>Social Connecting</h3>
                        <div class="social-icons">
                            <ul>
                                <li class="facebook"><a href="<?php echo $setting->getSettingData('type', 'social_facebook')->value; ?>" target="_blank"><span> </span></a></li>
                                <li class="twitter"><a href="<?php echo $setting->getSettingData('type', 'social_twitter')->value; ?>" target="_blank"><span> </span></a></li>
                                <li class="linkedin"><a href="<?php echo $setting->getSettingData('type', 'social_linkedin')->value; ?>" target="_blank"><span> </span></a></li>
                                <li class="youtube"><a href="<?php echo $setting->getSettingData('type', 'social_youtube')->value; ?>" target="_blank"><span> </span></a></li>
                                <li class="instagram"><a href="<?php echo $setting->getSettingData('type', 'social_instagram')->value; ?> target="_blank""><span> </span></a></li>
                            </ul>
                        </div>
                        <div class="follow">
                            <?php
                                if(isset($_POST['subscribe'])) {
                                    $addSubscribe = $subscribe->addSubscriber($_POST);
                                    if ($addSubscribe) {
                                        echo "<label style='color: green;'><strong>Thanks for subscribe us!.</strong></label>";
                                    } else {
                                        echo "<label style='color: red;'><strong>Sory, cant add new subscriber yet..</strong></label>";
                                    }
                                }
                            ?>
                            <p>Follow us and get the latest news by submit your email here.</p>
                            <div class="search">
                                <form action="" method="post">
                                    <input type="text" name="email" placeholder="your@email.com">
                                    <input type="submit" name="subscribe" value="">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col_1_of_3 span_1_of_3">
                        <h3>Contact info</h3>
                        <div class="footer-list">
                            <ul>
                                <li><img src="images/address.png" alt="" />
                                    <p><?php echo $setting->getSettingData('type', 'address')->value; ?></p>
                                    <div class="clear"></div>
                                </li>
                                <li><img src="images/phone.png" alt="" />
                                    <p>Phone: <?php echo $setting->getSettingData('type', 'phone')->value; ?></p>
                                    <div class="clear"></div>
                                </li>
                                <li><img src="images/msg.png" alt="" />
                                    <p>Email: <a class="no_link"><?php echo $setting->getSettingData('type', 'email')->value; ?></a></p>
                                    <div class="clear"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="wrap">
                <div class="copy">
                    <p class="copy">© 2019 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a>. Modified by <a href="https://www.linkedin.com/in/tisnadinata" target="_blank">MAT</a></p>
                </div>
                <div class="footer-nav">
                    <ul>
                        <li><a href="./home">Home</a></li>
                        <li><a href="./portfolio">Portfolio</a></li>
                        <li><a href="./blog">Blog</a></li>
                        <li><a href="./about">About Us</a></li>
                        <li><a href="./contact">Contact</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</body>
</html>